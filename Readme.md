# About this git and how to use
Files Contains
 - test.c - trapezoid code in sequential
 - test_parall.c - trapezoid code in parallel using MPI
 - TrapzoidResult - pdf file about compare time result between 2 files

 ## How to run code
  **test.c**
    
    Compile
        gcc test.c -o test
    Run
        ./test arguments1 arguments2 arguments3

**test_parall.c**

    Compile
        mpicc test_parall.c -o test_parall
    Run
        mpiexec -np [number process] ./test_parall arguments1 arguments2 arguments
    or
        mpirun -np [number process] ./test_parall arguments1 arguments2 arguments3